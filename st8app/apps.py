from django.apps import AppConfig


class St8AppConfig(AppConfig):
    name = 'st8app'
